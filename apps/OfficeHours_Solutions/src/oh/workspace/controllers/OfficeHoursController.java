package oh.workspace.controllers;

import djf.modules.AppGUIModule;
import static djf.modules.AppGUIModule.ENABLED;
import djf.ui.AppNodesBuilder;
import djf.ui.dialogs.AppDialogsFacade;
import java.util.ArrayList;
import java.util.Iterator;
import javafx.geometry.Insets;
import java.util.Optional;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonBar.ButtonData;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Dialog;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TablePosition;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.TextInputDialog;
import javafx.scene.control.ToggleGroup;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;
import javafx.util.Pair;
import javax.json.Json;
import javax.json.JsonObject;
import oh.OfficeHoursApp;
import static oh.OfficeHoursPropertyType.OH_ADD_TA_BUTTON;
import static oh.OfficeHoursPropertyType.OH_ALL_RADIO_BUTTON;
import static oh.OfficeHoursPropertyType.OH_EMAIL_TEXT_FIELD;
import static oh.OfficeHoursPropertyType.OH_FOOLPROOF_SETTINGS;
import static oh.OfficeHoursPropertyType.OH_GRADUATE_RADIO_BUTTON;
import static oh.OfficeHoursPropertyType.OH_NAME_TEXT_FIELD;
import static oh.OfficeHoursPropertyType.OH_NO_TA_SELECTED_CONTENT;
import static oh.OfficeHoursPropertyType.OH_NO_TA_SELECTED_TITLE;
import static oh.OfficeHoursPropertyType.OH_OFFICE_HOURS_TABLE_VIEW;
import static oh.OfficeHoursPropertyType.OH_TAS_TABLE_VIEW;
import static oh.OfficeHoursPropertyType.OH_UNDERGRADUATE_RADIO_BUTTON;
import oh.data.OfficeHoursData;
import oh.data.TeachingAssistantPrototype;
import oh.data.TimeSlot;
import oh.data.TimeSlot.DayOfWeek;
import oh.transactions.AddTA_Transaction;
import oh.transactions.ToggleOfficeHours_Transaction;
import static oh.workspace.style.OHStyle.CLASS_OH_TEXT_FIELD;

/**
 *
 * @author McKillaGorilla
 */
public class OfficeHoursController {

    OfficeHoursApp app;

    public OfficeHoursController(OfficeHoursApp initApp) {
        app = initApp;
    }

    public void processAddTA() {
        AppGUIModule gui = app.getGUIModule();
        TableView tasTable = (TableView) gui.getGUINode(OH_TAS_TABLE_VIEW);
        TableView rightTable = (TableView) gui.getGUINode(OH_OFFICE_HOURS_TABLE_VIEW);
        RadioButton ugradButton = (RadioButton) gui.getGUINode(OH_UNDERGRADUATE_RADIO_BUTTON);
        RadioButton gradButton = (RadioButton) gui.getGUINode(OH_GRADUATE_RADIO_BUTTON);
        
        TextField nameTF = (TextField) gui.getGUINode(OH_NAME_TEXT_FIELD);
        String name = nameTF.getText();
        TextField emailTF = (TextField) gui.getGUINode(OH_EMAIL_TEXT_FIELD);
        String email = emailTF.getText();
        OfficeHoursData data = (OfficeHoursData) app.getDataComponent();
        if (data.isLegalNewTA(name, email)) {
            TeachingAssistantPrototype ta = new TeachingAssistantPrototype(name.trim(), email.trim());
            if(ugradButton.isSelected()){
                ta.setType("Undergraduate");
            }
            else if(gradButton.isSelected()){
                ta.setType("Graduate");
            }
            else
                System.out.println("neither valid button is selected");
            
            AddTA_Transaction addTATransaction = new AddTA_Transaction(data, ta);
            app.processTransaction(addTATransaction);

            tasTable.refresh();
            // NOW CLEAR THE TEXT FIELDS
            nameTF.setText("");
            emailTF.setText("");
            nameTF.requestFocus();
        }
        
        tasTable.refresh();
        rightTable.refresh();
    }
    
    public void processAddTypeTA(String type) {
        AppGUIModule gui = app.getGUIModule();
        TextField nameTF = (TextField) gui.getGUINode(OH_NAME_TEXT_FIELD);
        String name = nameTF.getText();
        TextField emailTF = (TextField) gui.getGUINode(OH_EMAIL_TEXT_FIELD);
        String email = emailTF.getText();
        OfficeHoursData data = (OfficeHoursData) app.getDataComponent();
        if (data.isLegalNewTA(name, email)) {
            //nameTF.setStyle("-fx-text-fill: blue");
            TeachingAssistantPrototype ta = new TeachingAssistantPrototype(name.trim(), email.trim(), type.trim());
            AddTA_Transaction addTATransaction = new AddTA_Transaction(data, ta);
            app.processTransaction(addTATransaction);

            // NOW CLEAR THE TEXT FIELDS
            nameTF.setText("");
            emailTF.setText("");
            nameTF.requestFocus();
        }
    }
    

    public void processVerifyTA() {

    }

    public void processToggleOfficeHours() {
        AppGUIModule gui = app.getGUIModule();
        TableView<TimeSlot> officeHoursTableView = (TableView) gui.getGUINode(OH_OFFICE_HOURS_TABLE_VIEW);
        ObservableList<TablePosition> selectedCells = officeHoursTableView.getSelectionModel().getSelectedCells();
        if (selectedCells.size() > 0) {
            TablePosition cell = selectedCells.get(0);
            int cellColumnNumber = cell.getColumn();
            OfficeHoursData data = (OfficeHoursData)app.getDataComponent();
            if (data.isDayOfWeekColumn(cellColumnNumber)) {
                DayOfWeek dow = data.getColumnDayOfWeek(cellColumnNumber);
                TableView<TeachingAssistantPrototype> taTableView = (TableView)gui.getGUINode(OH_TAS_TABLE_VIEW);
                TeachingAssistantPrototype ta = taTableView.getSelectionModel().getSelectedItem();
                if (ta != null) {
                    TimeSlot timeSlot = officeHoursTableView.getSelectionModel().getSelectedItem();
                    ToggleOfficeHours_Transaction transaction = new ToggleOfficeHours_Transaction(data, timeSlot, dow, ta);
                    app.processTransaction(transaction);
                }
                else {
                    Stage window = app.getGUIModule().getWindow();
                    AppDialogsFacade.showMessageDialog(window, OH_NO_TA_SELECTED_TITLE, OH_NO_TA_SELECTED_CONTENT);
                }
            }
            int row = cell.getRow();
            cell.getTableView().refresh();
        }
    }

    public void processTypeTA() {
        app.getFoolproofModule().updateControls(OH_FOOLPROOF_SETTINGS);
    }
    
    public void printAllTas(){
        AppGUIModule gui = app.getGUIModule();
        OfficeHoursData dataManager = (OfficeHoursData) app.getDataComponent();
        ObservableList<TeachingAssistantPrototype> all = dataManager.getTaList();
        for(TeachingAssistantPrototype ta: all){
            System.out.println(ta.getName().toString() + " is of type " + ta.getType());
        }
        System.out.println("ends here \n");
    }
    
    public void processShowTaTable(String type){
        AppGUIModule gui = app.getGUIModule();
        OfficeHoursData dataManager = (OfficeHoursData) app.getDataComponent();
        TableView<TeachingAssistantPrototype> leftTable = (TableView) gui.getGUINode(OH_TAS_TABLE_VIEW);
        TableView rightTable = (TableView) gui.getGUINode(OH_OFFICE_HOURS_TABLE_VIEW);
        Button addTa = (Button) gui.getGUINode(OH_ADD_TA_BUTTON);
        
        RadioButton allButton = (RadioButton) gui.getGUINode(OH_ALL_RADIO_BUTTON);
        RadioButton ugButton = (RadioButton) gui.getGUINode(OH_UNDERGRADUATE_RADIO_BUTTON);
        RadioButton grButton = (RadioButton) gui.getGUINode(OH_GRADUATE_RADIO_BUTTON);
        
        ObservableList<TeachingAssistantPrototype> all = dataManager.getTaList();
        ObservableList<TeachingAssistantPrototype> ug = FXCollections.observableArrayList();
        ObservableList<TeachingAssistantPrototype> gr = FXCollections.observableArrayList();
        //gr = FXCollections.sort();
        if(type.equals("all")){
            addTa.setDisable(ENABLED);
            System.out.println(""); 
            System.out.println("List of all TAs");
            for(TeachingAssistantPrototype ta: all){
                System.out.println(ta.getName());
            }
            
            leftTable.setItems(all);
            
            Iterator<TimeSlot> timeSlotsIterator = dataManager.officeHoursIterator();
            while (timeSlotsIterator.hasNext()) {
                TimeSlot timeSlot = timeSlotsIterator.next();
                for (int i = 0; i < DayOfWeek.values().length; i++) {
                    DayOfWeek dow = DayOfWeek.values()[i];
                    if(timeSlot.getDayText().get(dow) != null){
                        StringProperty names = timeSlot.getDayText().get(dow);
                        names.setValue("");
                        ArrayList<TeachingAssistantPrototype> tas = timeSlot.getBackEndTa().get(dow);
                        
                        for(TeachingAssistantPrototype ta: tas){
                            if(all.contains(ta)){
                                names.setValue(names.getValue()+ta.getName());
                            }
                        }
                    }
                    else
                        continue;
                }
            }
            leftTable.refresh();
        }
        else if(type.equals("ug")){
            addTa.setDisable(false);
            processAddTA();
            //for(TeachingAssistantPrototype ta: taTableView.getItems().){
            for(TeachingAssistantPrototype ta: all){
                if(ta.getType().equals("Undergraduate")){
                    System.out.println(ta.getName() + " isUndergraduate");
                    ug.add(ta);
                }
            }
            leftTable.setItems(ug);
            leftTable.refresh();
            
            Iterator<TimeSlot> timeSlotsIterator = dataManager.officeHoursIterator();
            while (timeSlotsIterator.hasNext()) {
                TimeSlot timeSlot = timeSlotsIterator.next();
                for (int i = 0; i < DayOfWeek.values().length; i++) {
                    DayOfWeek dow = DayOfWeek.values()[i];
                    if(timeSlot.getDayText().get(dow) != null){
                        StringProperty names = timeSlot.getDayText().get(dow);
                        names.setValue("");
                        ArrayList<TeachingAssistantPrototype> tas = timeSlot.getBackEndTa().get(dow);
                        
                        for(TeachingAssistantPrototype ta: tas){
                            if(ug.contains(ta)){
                                names.setValue(names.getValue()+ta.getName());
                            }
                        }
                    }
                    else
                        continue;
                }
            }
            leftTable.refresh();
            rightTable.refresh();
            
            
        }
        else if(type.equals("gr")){
            addTa.setDisable(false);
            processAddTA();
            for(TeachingAssistantPrototype ta: all){
                if(ta.getType().equals("Graduate")){
                    gr.add(ta);
                    System.out.println(ta.getName() + " is Graduate");
                }
            }
            leftTable.setItems(gr);
            leftTable.refresh();
            System.out.println("\n");
            
            
            //FIXING THE RIGHT TABLE SO IT MATCHES WHATEVER IS IN THE RIGHT TABLE
            Iterator<TimeSlot> timeSlotsIterator = dataManager.officeHoursIterator();
            while (timeSlotsIterator.hasNext()) {
                TimeSlot timeSlot = timeSlotsIterator.next();
                for (int i = 0; i < DayOfWeek.values().length; i++) {
                    DayOfWeek dow = DayOfWeek.values()[i];
                    if(timeSlot.getDayText().get(dow) != null){
                        StringProperty names = timeSlot.getDayText().get(dow);
                        names.setValue("");
                        ArrayList<TeachingAssistantPrototype> tas = timeSlot.getBackEndTa().get(dow);
                        
                        for(TeachingAssistantPrototype ta: tas){
                            if(gr.contains(ta)){
                                names.setValue(names.getValue()+ta.getName());
                            }
                        }
                    }
                    else
                        continue;
                }
            }
            //gr = FXCollections.sort;
            leftTable.refresh();
        }
    }
    
    public void processHighlightNames(){
        AppGUIModule gui = app.getGUIModule();
        TableView<TeachingAssistantPrototype> leftTable = (TableView) gui.getGUINode(OH_TAS_TABLE_VIEW);
        TeachingAssistantPrototype taInCell = leftTable.getSelectionModel().getSelectedItem();
        
        TableView<TimeSlot> rightTable = (TableView) gui.getGUINode(OH_OFFICE_HOURS_TABLE_VIEW);

        OfficeHoursData dataManager = (OfficeHoursData)app.getDataComponent();
        //Iterator<TeachingAssistantPrototype> tasIterator = dataManager.teachingAssistantsIterator();
        rightTable.getSelectionModel().clearSelection();
        if (taInCell != null) {
            String taName = taInCell.getName();
            System.out.println(taName);
            
            int timeSlotNumber = 0;
            Iterator<TimeSlot> timeSlotsIterator = dataManager.officeHoursIterator();
            while (timeSlotsIterator.hasNext()) {
                TimeSlot timeSlot = timeSlotsIterator.next();
                for (int i = 0; i < DayOfWeek.values().length; i++) {
                    DayOfWeek dow = DayOfWeek.values()[i];
                    if(timeSlot.getDayText().get(dow) != null){
                        StringProperty names = timeSlot.getDayText().get(dow);
                        if(names.get().toLowerCase().contains(taName.toLowerCase())){
                            //System.out.println(dow + "contains");
                            //THIS IS THE CELL THAT SHOULD BE COLLORED
                            //names.setValue("lalala");
                            rightTable.getSelectionModel().select(timeSlotNumber, rightTable.getColumns().get(i+2));
                        }
                        else{
                            //System.out.println(dow + " does not contain " +taName);
                        }
                    }
                    else
                        continue;
                }
                timeSlotNumber++;
            }
        rightTable.refresh();
        }
        else {
            Stage window = app.getGUIModule().getWindow();
            AppDialogsFacade.showMessageDialog(window, OH_NO_TA_SELECTED_TITLE, OH_NO_TA_SELECTED_CONTENT);
        }
    }
    
    
    public void processEdit(){
        AppGUIModule gui = app.getGUIModule();
        TableView<TeachingAssistantPrototype> leftTable = (TableView) gui.getGUINode(OH_TAS_TABLE_VIEW);
        TeachingAssistantPrototype taInCell = leftTable.getSelectionModel().getSelectedItem();
        String taName = taInCell.getName();
        String taEmail = taInCell.getEmail();
        String taType = taInCell.getType();
        
        Dialog<ButtonType> editTA = new Dialog<>();
        editTA.setTitle("Edit " + taName);
        editTA.setHeaderText("Edit " +taName);
        
        ButtonType makeChanges = new ButtonType("MAKE CHANGES", ButtonData.OK_DONE);
        editTA.getDialogPane().getButtonTypes().addAll(makeChanges, ButtonType.CANCEL);

        
        GridPane grid = new GridPane();
        grid.setHgap(10);
        grid.setVgap(10);
        grid.setPadding(new Insets(10,10,10,10));
        
        TextField newName = new TextField();
        newName.setPromptText("Name");
        newName.setText(taName);
        TextField newEmail = new TextField();
        newEmail.setPromptText("Email");
        newEmail.setText(taEmail);
        
        
        final ToggleGroup group = new ToggleGroup();
        RadioButton ug = new RadioButton();
        ug.setText("Undergraduate");
        ug.setToggleGroup(group);
        RadioButton gr = new RadioButton();
        gr.setText("Graduate");
        gr.setToggleGroup(group);
        //RadioButton third = new RadioButton("Example");  // Alternative way of declaring button
        if(taType.equals("Undergraduate")){
            ug.setSelected(ENABLED);
            ug.requestFocus();
        }
        else{
            gr.setSelected(ENABLED);
            gr.requestFocus();
        }

        grid.add(new Label("Name: "),0,0);
        grid.add(newName, 1, 0);
        grid.add(new Label("Email: "),0,1);
        grid.add(newEmail, 1, 1);
        grid.add(new Label("Type: "), 0, 2);
        grid.add(ug, 1, 2);
        grid.add(gr, 2, 2);

       
        //Node editButton = editTA.getDialogPane().lookupButton(ButtonType.CLOSE);
        //editButton.setDisable(true);
        
        editTA.getDialogPane().setContent(grid);

        newName.textProperty().addListener(e ->{
            processTypeTA();
        });
        newEmail.textProperty().addListener(e ->{
            processTypeTA();
        });
        
        /**
         * 
         * ADD RED DESIGN IF NOT VALID NAME AND EMAIL
         */
        
        
        Optional<ButtonType> result = editTA.showAndWait();
        if(result.get()==makeChanges){
            System.out.println("MakeChanges Selected");
            System.out.println("Name before change: " + taInCell.getName());
            taInCell.setName(newName.getText());
            System.out.println("Name after: " + taInCell.getName());
            taInCell.setEmail(newEmail.getText());
            if(ug.isSelected()){
                System.out.println("Undergrad selected");
                taInCell.setType("Undergraduate");
            }
            else if(gr.isSelected()){
                System.out.println("Grad selected");
                taInCell.setType("Graduate");
            }
        }
        else{
            System.out.println("Canceled Changes");
        }

        leftTable.refresh();
        
    }
    
    
    
    public void cutTA(){
        AppGUIModule gui = app.getGUIModule();
        
    }
    public void copyTA(){
        AppGUIModule gui = app.getGUIModule();
        
    }
    public void pasteTA(){
        AppGUIModule gui = app.getGUIModule();
        
    }

}
