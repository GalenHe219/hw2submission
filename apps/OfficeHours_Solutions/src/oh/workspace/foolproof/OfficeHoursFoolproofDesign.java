package oh.workspace.foolproof;

import djf.modules.AppGUIModule;
import djf.ui.foolproof.FoolproofDesign;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import oh.OfficeHoursApp;
import static oh.OfficeHoursPropertyType.OH_ADD_TA_BUTTON;
import static oh.OfficeHoursPropertyType.OH_EMAIL_TEXT_FIELD;
import static oh.OfficeHoursPropertyType.OH_NAME_TEXT_FIELD;
import oh.data.OfficeHoursData;

/**
 *
 * @author McKillaGorilla
 */
public class OfficeHoursFoolproofDesign implements FoolproofDesign {
    OfficeHoursApp app;
    
    public OfficeHoursFoolproofDesign(OfficeHoursApp initApp) {
        app = initApp;
    }

    @Override
    public void updateControls() {
        updateAddTAFoolproofDesign();
    }

    public void updateAddTAFoolproofDesign() {
        AppGUIModule gui = app.getGUIModule();
        // FOOLPROOF DESIGN STUFF FOR ADD TA BUTTON
        TextField nameTextField = ((TextField) gui.getGUINode(OH_NAME_TEXT_FIELD));
        TextField emailTextField = ((TextField) gui.getGUINode(OH_EMAIL_TEXT_FIELD));
        String name = nameTextField.getText();
        String email = emailTextField.getText();
        OfficeHoursData data = (OfficeHoursData) app.getDataComponent();
        Button addTAButton = (Button)gui.getGUINode(OH_ADD_TA_BUTTON);
        boolean isLegal = data.isLegalNewTA(name, email);
        boolean legalName = data.isLegalName(name);
        boolean legalEmail = data.isLegalNewEmail(email);
        
        if (isLegal) {
            nameTextField.setOnAction(addTAButton.getOnAction());
            emailTextField.setOnAction(addTAButton.getOnAction());
        }
        else {
            nameTextField.setOnAction(null);
            emailTextField.setOnAction(null);
            nameTextField.setStyle("-fx-text-fill: red");
            emailTextField.setStyle("-fx-text-fill: red");
        }
        if (legalName){
            nameTextField.setStyle("-fx-text-fill: black");
        }
        if(legalEmail){
            emailTextField.setStyle("-fx-text-fill: black");
        }
        addTAButton.setDisable(!isLegal);
    }
}