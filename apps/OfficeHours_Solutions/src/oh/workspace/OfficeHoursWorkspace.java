package oh.workspace;

import static djf.AppPropertyType.COPY_BUTTON;
import static djf.AppPropertyType.CUT_BUTTON;
import static djf.AppPropertyType.PASTE_BUTTON;
import djf.components.AppClipboardComponent;
import djf.components.AppWorkspaceComponent;
import djf.modules.AppFoolproofModule;
import djf.modules.AppGUIModule;
import static djf.modules.AppGUIModule.ENABLED;
import djf.ui.AppNodesBuilder;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.RadioButton;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.SplitPane;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.Toggle;
import javafx.scene.control.ToggleGroup;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import properties_manager.PropertiesManager;
import oh.OfficeHoursApp;
import oh.OfficeHoursPropertyType;
import static oh.OfficeHoursPropertyType.*;
import oh.clipboard.OfficeHoursClipboard;
import oh.data.OfficeHoursData;
import oh.data.TeachingAssistantPrototype;
import oh.data.TimeSlot;
import oh.workspace.controllers.OfficeHoursController;
import oh.workspace.foolproof.OfficeHoursFoolproofDesign;
import static oh.workspace.style.OHStyle.*;

/**
 *
 * @author McKillaGorilla
 */
public class OfficeHoursWorkspace extends AppWorkspaceComponent {

    public OfficeHoursWorkspace(OfficeHoursApp app) {
        super(app);

        // LAYOUT THE APP
        initLayout();

        // INIT THE EVENT HANDLERS
        initControllers();

        // INIT THE FOOLPROOF STUFF
        initFoolproofDesign();
    }

    // THIS HELPER METHOD INITIALIZES ALL THE CONTROLS IN THE WORKSPACE
    private void initLayout() {
        // FIRST LOAD THE FONT FAMILIES FOR THE COMBO BOX
        PropertiesManager props = PropertiesManager.getPropertiesManager();

        // THIS WILL BUILD ALL OF OUR JavaFX COMPONENTS FOR US
        AppNodesBuilder ohBuilder = app.getGUIModule().getNodesBuilder();

        // INIT THE HEADER ON THE LEFT
        VBox leftPane = ohBuilder.buildVBox(OH_LEFT_PANE, null, CLASS_OH_PANE, ENABLED);
        
        HBox tasHeaderBox = ohBuilder.buildHBox(OH_TAS_HEADER_PANE, leftPane, CLASS_OH_BOX, ENABLED); //RADIO BUTTONS GO HERE
        ohBuilder.buildLabel(OfficeHoursPropertyType.OH_TAS_HEADER_LABEL, tasHeaderBox, CLASS_OH_HEADER_LABEL, ENABLED);
        //ohBuilder.buildLabel(OfficeHoursPropertyType.OH_TAS_RADIO_BUTTON, tasHeaderBox, CLASS_OH_RADIO_BUTTON, ENABLED);
        //ohBuilder.buildTextToggleButton(OH_TAS_RADIO_BUTTON, tasHeaderBox, "hello", ENABLED);
        //ohBuilder.buildIconButton(OH_TAS_RADIO_BUTTON, tasHeaderBox, CLASS_OH_RADIO_BUTTON, ENABLED);
        //ohBuilder.buildRadio(OH_TAS_RADIO_BUTTON, tasHeaderBox, CLASS_OH_RADIO_BUTTON, ENABLED);
        ohBuilder.buildRadio(OH_UNDERGRADUATE_RADIO_BUTTON, tasHeaderBox, CLASS_OH_RADIO_BUTTON, ENABLED);
        ohBuilder.buildRadio(OH_GRADUATE_RADIO_BUTTON, tasHeaderBox, CLASS_OH_RADIO_BUTTON, ENABLED);
        ohBuilder.buildRadio(OH_ALL_RADIO_BUTTON, tasHeaderBox, CLASS_OH_RADIO_BUTTON, ENABLED);
       
        tasHeaderBox.setPadding(new Insets(10,10,10,10));
        
        OfficeHoursController controller = new OfficeHoursController((OfficeHoursApp) app);
        
        // MAKE THE TABLE AND SETUP THE DATA MODEL
        TableView<TeachingAssistantPrototype> taTable = ohBuilder.buildTableView(OH_TAS_TABLE_VIEW, leftPane, CLASS_OH_TABLE_VIEW, ENABLED);
        taTable.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);
        TableColumn nameColumn = ohBuilder.buildTableColumn(OH_NAME_TABLE_COLUMN, taTable, CLASS_OH_COLUMN);
        TableColumn emailColumn = ohBuilder.buildTableColumn(OH_EMAIL_TABLE_COLUMN, taTable, CLASS_OH_COLUMN);
        TableColumn slotsColumn = ohBuilder.buildTableColumn(OH_SLOTS_TABLE_COLUMN, taTable, CLASS_OH_CENTERED_COLUMN);
        TableColumn typeColumn = ohBuilder.buildTableColumn(OH_TYPE_TABLE_COLUMN, taTable, CLASS_OH_COLUMN);
        nameColumn.setCellValueFactory(new PropertyValueFactory<String, String>("name"));
        emailColumn.setCellValueFactory(new PropertyValueFactory<String, String>("email"));
        slotsColumn.setCellValueFactory(new PropertyValueFactory<String, String>("slots"));
        typeColumn.setCellValueFactory(new PropertyValueFactory<String, String>("type"));
        nameColumn.prefWidthProperty().bind(taTable.widthProperty().multiply(1.0/4.0));
        emailColumn.prefWidthProperty().bind(taTable.widthProperty().multiply(1.0/4.0));
        slotsColumn.prefWidthProperty().bind(taTable.widthProperty().multiply(1.0/4.0));
        typeColumn.prefWidthProperty().bind(taTable.widthProperty().multiply(1.0/4.0));
        
        // ADD BOX FOR ADDING A TA
        HBox taBox = ohBuilder.buildHBox(OH_ADD_TA_PANE, leftPane, CLASS_OH_PANE, ENABLED);
        ohBuilder.buildTextField(OH_NAME_TEXT_FIELD, taBox, CLASS_OH_TEXT_FIELD, ENABLED);
        ohBuilder.buildTextField(OH_EMAIL_TEXT_FIELD, taBox, CLASS_OH_TEXT_FIELD, ENABLED);
        ohBuilder.buildTextButton(OH_ADD_TA_BUTTON, taBox, CLASS_OH_BUTTON, !ENABLED);
        
        // MAKE SURE IT'S THE TABLE THAT ALWAYS GROWS IN THE LEFT PANE
        VBox.setVgrow(taTable, Priority.ALWAYS);

        // INIT THE HEADER ON THE RIGHT
        VBox rightPane = ohBuilder.buildVBox(OH_RIGHT_PANE, null, CLASS_OH_PANE, ENABLED);
        HBox officeHoursHeaderBox = ohBuilder.buildHBox(OH_OFFICE_HOURS_HEADER_PANE, rightPane, CLASS_OH_PANE, ENABLED);
        ohBuilder.buildLabel(OH_OFFICE_HOURS_HEADER_LABEL, officeHoursHeaderBox, CLASS_OH_HEADER_LABEL, ENABLED);

        // SETUP THE OFFICE HOURS TABLE
        TableView<TimeSlot> officeHoursTable = ohBuilder.buildTableView(OH_OFFICE_HOURS_TABLE_VIEW, rightPane, CLASS_OH_OFFICE_HOURS_TABLE_VIEW, ENABLED);
        officeHoursTable.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
        TableColumn startTimeColumn = ohBuilder.buildTableColumn(OH_START_TIME_TABLE_COLUMN, officeHoursTable, CLASS_OH_TIME_COLUMN);
        TableColumn endTimeColumn = ohBuilder.buildTableColumn(OH_END_TIME_TABLE_COLUMN, officeHoursTable, CLASS_OH_TIME_COLUMN);
        TableColumn mondayColumn = ohBuilder.buildTableColumn(OH_MONDAY_TABLE_COLUMN, officeHoursTable, CLASS_OH_DAY_OF_WEEK_COLUMN);
        TableColumn tuesdayColumn = ohBuilder.buildTableColumn(OH_TUESDAY_TABLE_COLUMN, officeHoursTable, CLASS_OH_DAY_OF_WEEK_COLUMN);
        TableColumn wednesdayColumn = ohBuilder.buildTableColumn(OH_WEDNESDAY_TABLE_COLUMN, officeHoursTable, CLASS_OH_DAY_OF_WEEK_COLUMN);
        TableColumn thursdayColumn = ohBuilder.buildTableColumn(OH_THURSDAY_TABLE_COLUMN, officeHoursTable, CLASS_OH_DAY_OF_WEEK_COLUMN);
        TableColumn fridayColumn = ohBuilder.buildTableColumn(OH_FRIDAY_TABLE_COLUMN, officeHoursTable, CLASS_OH_DAY_OF_WEEK_COLUMN);
        startTimeColumn.setCellValueFactory(new PropertyValueFactory<String, String>("startTime"));
        endTimeColumn.setCellValueFactory(new PropertyValueFactory<String, String>("endTime"));
        mondayColumn.setCellValueFactory(new PropertyValueFactory<String, String>("monday"));
        tuesdayColumn.setCellValueFactory(new PropertyValueFactory<String, String>("tuesday"));
        wednesdayColumn.setCellValueFactory(new PropertyValueFactory<String, String>("wednesday"));
        thursdayColumn.setCellValueFactory(new PropertyValueFactory<String, String>("thursday"));
        fridayColumn.setCellValueFactory(new PropertyValueFactory<String, String>("friday"));
        for (int i = 0; i < officeHoursTable.getColumns().size(); i++) {
            ((TableColumn)officeHoursTable.getColumns().get(i)).prefWidthProperty().bind(officeHoursTable.widthProperty().multiply(1.0/7.0));
        }

        // MAKE SURE IT'S THE TABLE THAT ALWAYS GROWS IN THE LEFT PANE
        VBox.setVgrow(officeHoursTable, Priority.ALWAYS);

        // BOTH PANES WILL NOW GO IN A SPLIT PANE
        SplitPane sPane = new SplitPane(leftPane, rightPane);
        sPane.setDividerPositions(.4);
        workspace = new BorderPane();

        // AND PUT EVERYTHING IN THE WORKSPACE
        ((BorderPane)workspace).setCenter(sPane);
    }

    public void initControllers() {
        OfficeHoursController controller = new OfficeHoursController((OfficeHoursApp) app);
        OfficeHoursClipboard clipboard = new OfficeHoursClipboard ((OfficeHoursApp) app);
        AppGUIModule gui = app.getGUIModule();
        
        // FOOLPROOF DESIGN STUFF
        TextField nameTextField = ((TextField) gui.getGUINode(OH_NAME_TEXT_FIELD));
        TextField emailTextField = ((TextField) gui.getGUINode(OH_EMAIL_TEXT_FIELD));
        
        nameTextField.textProperty().addListener(e->{
            controller.processTypeTA();
        });
        emailTextField.textProperty().addListener(e->{
            controller.processTypeTA();
        });
        
        // FIRE THE ADD EVENT ACTION
        nameTextField.setOnAction(e -> {
            controller.processAddTA();
        });
        emailTextField.setOnAction(e -> {
            controller.processAddTA();
        });
        ((Button) gui.getGUINode(OH_ADD_TA_BUTTON)).setOnAction(e -> {
            controller.processAddTA();
        });
        
        
        //Adding office hours for a given TA
        TableView officeHoursTableView = (TableView) gui.getGUINode(OH_OFFICE_HOURS_TABLE_VIEW);
        officeHoursTableView.getSelectionModel().setCellSelectionEnabled(true);
        officeHoursTableView.setOnMouseClicked(e -> {
            controller.processToggleOfficeHours();
        });

        
        //Toggling the TA table based on which Radio button (undergrad, grad, all) is pressed. 
        Button cutButton = (Button)gui.getGUINode(CUT_BUTTON);
        Button copyButton = (Button)gui.getGUINode(COPY_BUTTON);
        Button pasteButton = (Button)gui.getGUINode(PASTE_BUTTON);
        
        final ToggleGroup group = new ToggleGroup();
        RadioButton ugradButton = (RadioButton) gui.getGUINode(OH_UNDERGRADUATE_RADIO_BUTTON);
        RadioButton gradButton = (RadioButton) gui.getGUINode(OH_GRADUATE_RADIO_BUTTON);
        RadioButton allButton = (RadioButton) gui.getGUINode(OH_ALL_RADIO_BUTTON);
        
        ugradButton.setToggleGroup(group);
        ugradButton.setUserData("ug");
        ugradButton.setOnMousePressed(e -> {
            //controller.printAllTas();
            pasteButton.setDisable(true);
            controller.processShowTaTable("ug");
            controller.processAddTA();
            
        });
        
        gradButton.setToggleGroup(group);
        gradButton.setUserData("ug");
        gradButton.setOnMousePressed(e -> {
            pasteButton.setDisable(true);
            //controller.printAllTas();
            controller.processShowTaTable("gr");
            controller.processAddTA();
            
        });
        
        allButton.setToggleGroup(group);
        allButton.setUserData("ug");
        allButton.setSelected(ENABLED);
        allButton.requestFocus();
        allButton.setOnMousePressed(e -> {
            AppClipboardComponent cb = app.getClipboardComponent();
            if(cb.hasSomethingToPaste()){
                System.out.println("has something");
                pasteButton.setDisable(false);
            }
            else
                System.out.println("has nothing");
            controller.processShowTaTable("all");
            controller.processAddTA();
        });
        
        // Attempt to highlight the TA in the OH right table based on the click on the left
        TableView tasTable = (TableView) gui.getGUINode(OH_TAS_TABLE_VIEW);
        tasTable.setOnMouseClicked(new EventHandler<MouseEvent>(){
            @Override
            public void handle(MouseEvent mouseEvent){
                if(mouseEvent.getButton().equals(MouseButton.PRIMARY)){
                    if(mouseEvent.getClickCount()==1){
                        Button cutButton = (Button)gui.getGUINode(CUT_BUTTON);
                        cutButton.setDisable(false);
                        Button copyButton = (Button)gui.getGUINode(COPY_BUTTON);
                        copyButton.setDisable(false);
                        System.out.print("Highlighted Ta: ");
                        System.out.println("");
                        controller.processHighlightNames();
                    }
                    if(mouseEvent.getClickCount()==2){
                        controller.processEdit();
                        controller.processAddTA();
                    }
                }
            }
        });
        
        
        cutButton.setOnMouseClicked(e -> {
            pasteButton.setDisable(false);
        });
        copyButton.setOnMouseClicked(e -> {
            pasteButton.setDisable(false);
        });
        pasteButton.setOnMouseClicked(e -> {
        });
        
        
        // DON'T LET ANYONE SORT THE TABLES
        TableView tasTableView = (TableView)gui.getGUINode(OH_TAS_TABLE_VIEW);
        for (int i = 0; i < officeHoursTableView.getColumns().size(); i++) {
            ((TableColumn)officeHoursTableView.getColumns().get(i)).setSortable(false);
        }
        for (int i = 0; i < tasTableView.getColumns().size(); i++) {
            ((TableColumn)tasTableView.getColumns().get(i)).setSortable(false);
        }
    }

    public void initFoolproofDesign() {
        AppGUIModule gui = app.getGUIModule();
        AppFoolproofModule foolproofSettings = app.getFoolproofModule();
        foolproofSettings.registerModeSettings(OH_FOOLPROOF_SETTINGS,
                new OfficeHoursFoolproofDesign((OfficeHoursApp) app));
    }

    @Override
    public void processWorkspaceKeyEvent(KeyEvent ke) {
        // WE AREN'T USING THIS FOR THIS APPLICATION
    }

    @Override
    public void showNewDialog() {
        // WE AREN'T USING THIS FOR THIS APPLICATION
    }
}
