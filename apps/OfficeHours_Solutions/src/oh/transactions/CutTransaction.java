/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package oh.transactions;

import jtps.jTPS_Transaction;
import oh.data.OfficeHoursData;
import oh.data.TeachingAssistantPrototype;

/**
 *
 * @author Galen
 */
public class CutTransaction implements jTPS_Transaction {
    OfficeHoursData data;
    TeachingAssistantPrototype ta;
    
    public CutTransaction(OfficeHoursData initData, TeachingAssistantPrototype initTA){
        data = initData;
        ta = initTA;
    }
    
    @Override
    public void doTransaction() {
        data.removeTA(ta);        
    }

    @Override
    public void undoTransaction() {
        data.addTA(ta);
    }
}
